import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PizzaComponent } from "./pizza/pizza.component";

const routes: Routes = [
    { path: 'pizzashop5',     component: PizzaComponent },
];
@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}