export class Pizza{
    public id: number;
    public name: string;
    public description: string;
    public price: number;
}