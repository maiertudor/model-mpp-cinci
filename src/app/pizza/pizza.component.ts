import { Component, OnInit } from '@angular/core';
import { Pizza } from './pizza-model';
import { PizzaService } from './pizza.service';

@Component({
  selector: 'app-pizza',
  templateUrl: './pizza.component.html',
  styleUrls: ['./pizza.component.css']
})
export class PizzaComponent {
  showForm: boolean = false;
  pizza: Pizza = new Pizza();
  message: String = '';

  constructor(private pizzaService: PizzaService) { }

  onAddButton() {
    this.showForm = true;
  }

  onSaveButton() {
    this.pizzaService.addPizza(this.pizza).subscribe(
      data => this.showMessage(data)
    );
  }

  showMessage(data) {
    this.showForm = false;
    console.log(data);
    this.message = data._body;
  }

}
