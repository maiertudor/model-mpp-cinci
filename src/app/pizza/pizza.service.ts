import {Injectable} from '@angular/core';
import {Http, Response} from "@angular/http";

import {Observable} from "rxjs";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {Pizza} from "./pizza-model";

@Injectable()
export class PizzaService {
  private URL = 'http://localhost:8080/api/addPizza';

  constructor(private _http: Http) { }

  addPizza(pizza: Pizza): Observable<Response>{
    return this._http.post(this.URL, pizza);
  }

}
